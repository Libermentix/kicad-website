+++
title = "2020 Year End Funding Drive Results"
date = "2021-01-16"
draft = false
"blog/categories" = [
    "News"
]
+++

On January 15th, 2021 the KiCad 2020 year end funding drive officially
ended and it was a resounding success.  A total of $14,275 was raised by
individual donors which means the full $10,000 match was donated by the
https://www.kipro-pcb.com/[KiCad Services Corporation] making the total
donations for the funding drive $24,275.  Thank you to everyone who donated.
Your generosity is greatly appreciated and will help the project achieve
it's goals of getting the upcoming stable version 6 released as well as
kicking off development of version 7 in the new year.

The KiCad development team.
